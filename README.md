# NanoChat

A free/libre and open source alternative to SuperChat and other livestream chat applications.

*This project is a work in progress!*

It's not ready yet, be patient.

## The Idea

Use Nostr and Nano (XNO) to send paid messages to the streamer, signed with sender's Nano private key.

User message -> Signature -> Nostr -> Verification -> Streamer

On the streamer's side, a bot listens to incoming messages and incoming payments.

It goes through all the messages looking for the ones that the signature matches any of the Nano addresses (public keys) that sent Nano to the streamer.

## Usage Flow

### Streamer

1. Streamer download app
2. Streamer click "Start NanoChat"
    - App creates temporary Nano account (yes, one account per livestream, makes it harder to trace)
    - App creates temporary Nostr account (yes, one account per livestream, to prevent spam)
    - App start listening to incoming transactions
    - App looks, on Nostr, for message containing the signature for received transaction addresses (check if any of the Nano addresses (public keys) that sent them money matches with signed message)
3. Streamer end the livestream
4. Streamer enter their Nano Account (or a mixer, maybe?) in the "Nano Account" field
5. Streamer click "Receive Nano"
    - App sends them the received Nano from the temporary account


### User

1. User download app
    - App creates a Nano account on first use
    - App creates a Nostr account on first use
2. User send money to Nano account created on first use
3. User scan QR Code from livestream (containing streamer's Nano Address and Nostr Chat URL)
4. User set amount to send to the streamer
5. User type a message
6. User click "Send"
    - App sends the amount defined by the user to the streamer
    - App sends the message, to a Nostr Chat owned by the streamer, signed with Nano's private key
